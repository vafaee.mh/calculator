import './App.css';
import {useState} from "react";

function App() {
    const [calc, setCalc] = useState("")
    const [result, setResult] = useState("");
    const operator = ['/', '*', '+', '-', '.']
    const updateCalc = value => {
        if (

            operator.includes(value) && calc === '' ||
            operator.includes(value) && operator.includes(calc.slice(-1))
        ) {

            return
        }
        setCalc(calc + value)
        if (!operator.includes(value)) {
            setResult(eval(calc + value).toString())
        }
    }

    const createDigits = () => {
        const Digits = [];
        for (let i = 1; i < 10; i++) {
            Digits.push(
                <button key={i} onClick={() => updateCalc(i.toString())}>{i}</button>
            )
        }
        return Digits
    }
    const calculate = () => {
        setCalc(eval(calc).toString())
    }
    const deleteCalc=()=>{
        if (calc===''){
            return
        }
        const value=calc.slice(0,-1)
        setCalc(value)
    }
    return (
        <div className="App">
            <div className="Calculator">
                <div className="Display">
                    {result ? <span>({result})</span> : ''}

                    {calc || '0'}
                </div>
                <div className="Operators">
                    <button onClick={() => updateCalc('/')}>/</button>
                    <button onClick={() => updateCalc('*')}>*</button>
                    <button onClick={() => updateCalc('+')}>+</button>
                    <button onClick={() => updateCalc('-')}>-</button>
                    <button onClick={deleteCalc}>Del</button>
                </div>
                <div className="Digits">
                    {createDigits()}
                    <button onClick={() => updateCalc('0')}>0</button>
                    <button onClick={() => updateCalc('.')}>.</button>
                    <button onClick={calculate}>=</button>
                </div>
            </div>
        </div>
    );
}

export default App;
